out := bin

all: dirs $(out)/komppl $(out)/kompassr $(out)/absloadm

dirs:
	-mkdir -p $(out)

os := $(shell uname)
CFLAGS := -fpermissive -DOS_$(os)

$(info CFLAGS = $(CFLAGS))

$(out)/komppl: KOMPPL.CPP
	g++ -o $(out)/komppl $(CFLAGS) KOMPPL.CPP

$(out)/kompassr: KOMPASSR.CPP
	g++ -o $(out)/kompassr $(CFLAGS) KOMPASSR.CPP

$(out)/absloadm: ABSLOADM.CPP
	g++ -lncurses -o $(out)/absloadm $(CFLAGS) ABSLOADM.CPP

